require "./src/omnibus"

Omnibus.software :ruby do
  dependency :foo
end

Omnibus.software :foo do
  dependency :bar
  dependency :buz

  build do
    block do
      sleep 2
    end
  end
end

Omnibus.software :bar do
  dependency :owl

  build do
    block do
      puts "Longg bar"
      sleep 4
      puts "....still bar"
      sleep 4
      puts "...finishing"
    end
  end
end

Omnibus.software :owl do
  dependency :bob

  build do
    block do
      puts "start owl build"
      sleep 3
      puts "building owl....still"
      sleep 1
    end
  end
end

Omnibus.software :jack do
  dependency :bob

  build do
    block do
      sleep 1
      puts "JACK!"
    end
  end
end

Omnibus.software :buz do
end

Omnibus.software :bob do
  build do
    block do
      sleep 1
    end
  end
end


puts Omnibus.build
