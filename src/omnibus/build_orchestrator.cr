module Omnibus
  struct ColorIterator
    getter :current, :colors

    def initialize(@current = 0, @colors = [:red, :green, :yellow, :blue, :magenta, :cyan]); end

    def next
      if @colors[@current]?.nil?
        @current = 0
      end
      sym = @colors[@current]
      @current += 1
      sym
    end

    def rewind
      @current = 0
    end
  end

  struct BuildOrchestrator
    getter :graph, :registry, :to_build

    @to_build : Array(Symbol)

    def initialize(@graph : Graph, @registry : Registry, names : Array(Symbol)?)
      if names
        @to_build = get_to_build(names, graph).uniq
      else
        @to_build = graph.nodes.dup
      end
    end

    private def get_to_build(names, graph, new = names.dup, original = names.dup)
      return new if names.empty?
      name = names.shift
      vertex = graph.verticies[name]
      new = (new + vertex.incoming_names).uniq
      new.concat get_to_build((vertex.incoming_names - names), graph, new)

      get_to_build(names, graph, new)
    end

    def build(lookup = registry.to_groups, names = to_build, built = [] of Symbol)
      unblocked_channel = Channel(NamedTuple(names: Array(Symbol), built: Array(Symbol))).new
      built_channel = Channel(Array(Symbol)).new
      build_done = Channel(Symbol).new
      colors = ColorIterator.new
      # wait for deps
      # unblocker fiber
      spawn do
        loop do
          built = built_channel.receive

          unblocked = names.select do |name|
            vertex = graph.verticies[name]
            (vertex.incoming_names - built).size.zero? && !built.includes?(name)
          end

          blocked = names.select do |name|
            vertex = graph.verticies[name]
            (vertex.incoming_names - built).size > 0 && !built.includes?(name)
          end

          puts "#{blocked.join(", ")} still blocked"

          if unblocked.size > 0
            puts "#{unblocked.join(", ")} is unblocked"

            unblocked_channel.send({ names: unblocked, built: built })
          end
        end
      end

      # initial fiber for unblocked items
      spawn do
        unblocked = names.select do |name|
          vertex = graph.verticies[name]
          vertex.incoming_names.empty?
        end

        puts "Initial builds #{unblocked.join(", ")}"
        unblocked_channel.send({ names: unblocked, built: [] of Symbol })
      end

      # builder loop
      loop do
        # next group of things ready to build
        unblocked_hash = unblocked_channel.receive
        unblockeds = unblocked_hash[:names]
        built = unblocked_hash[:built]
        unblockeds.each_with_index do |unblocked, idx|
          software = lookup[unblocked]
          color = colors.next

          # build these things async
          spawn do
            puts "building #{unblocked}"
            software.do_build(color)
            puts "#{unblocked} is built"
            # send built
            build_done.send(unblocked)
          end
        end

        unblockeds.size.times do
          done = build_done.receive
          puts
        end

        built_channel.send(built.concat(unblockeds))

        break if (names - built).size.zero?
      end
    end
  end
end
