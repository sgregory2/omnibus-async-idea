require "./**"
module Omnibus
  module OS
    module Helper
      {% if flag?(:linux) %}
        include OS::Linux::Platform
      {% elsif flag?(:darwin) %}
        include OS::Darwin::Platform
      {% elsif flag?(:win32) %}
      {% elsif flag?(:freebsd) %}
      {% end %}

      def with_standard_compiler_flags(env = {} of String => String, opts = {} of String => String)
        compiler_flags = case platform
        else
          {
          "LDFLAGS" => "-Wl,-rpath,#{install_dir}/embedded/lib -L#{install_dir}/embedded/lib",
          "CFLAGS" => "-I#{install_dir}/embedded/include -O3 -D_FORTIFY_SOURCE=2 -fstack-protector"
          }
        end

        extra_linker_flags = {
        "LD_RUN_PATH" => "#{install_dir}/embedded/lib"
        }

        env
        .merge(compiler_flags)
        .merge(extra_linker_flags)
        .merge({ "PKG_CONFIG_PATH" => "#{install_dir}/embedded/lib/pkgconfig" })
        .merge({ "CXXFLAGS" => compiler_flags["CFLAGS"] })
        .merge({ "CPPFLAGS" => compiler_flags["CFLAGS"] })
        .merge({ "OMNIBUS_INSTALL_DIR" => install_dir })
      end

      def with_embedded_path(env = {} of String => String)
        paths = ["#{install_dir}/bin", "#{install_dir}/embedded/bin"]
        path_value = prepend_path(paths)
        env.merge({ path_key => path_value })
      end

      private def prepend_path(paths)
        paths << ENV[path_key]

        paths.join(":")
      end

      private def path_key
        "PATH"
      end
    end
  end
end
