module Omnibus
  module OS
    module Linux
      module Platform
        @@os_release_info : Hash(String, String)?

        protected def read_os_release_file(file)
          return nil unless File.exists?(file)

          puts File.read(file).split
          File.read(file).split.reduce({} of String => String) do |map, line|
            if line
              stuff = line.split("=")
              key = stuff[0]?
              value = stuff[1]?
              if key && value
                map[key] = value.gsub(/\A"|"\Z/, "") if value
              end
            end
            map
          end
        end

        protected def platform
          os_release_info.try { |f| f["ID"]? }
        end

        protected def os_release_info
          @@os_release_info ||= read_os_release_file("/etc/os-release")
        end
      end
    end
  end
end
