module Omnibus
  struct Compression
    getter :id

    def initialize(@id : Symbol); end
  end
end
