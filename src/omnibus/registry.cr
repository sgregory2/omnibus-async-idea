module Omnibus
  struct Registry
    getter :list


    def initialize(@list = [] of Software); end

    def <<(software)
      @list << software
    end

    def build
      ordered.build(to_groups)
    end

    def dag
      graph = Graph.new

      list.dup.each do |software|
        graph.add(software.name)

        software.dependencies.each do |name|
          graph.add_edge(name, software.name)
        end
      end

      graph
    end

    protected def to_groups
      list.reduce({} of Symbol => Software) do |memo, software|
        memo[software.name] = software
        memo
      end
    end
  end
end

