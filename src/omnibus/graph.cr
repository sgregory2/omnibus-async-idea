module Omnibus
  alias VisitorCallback = Proc(Vertex, Array(Symbol), Nil)

  def self.visit(
    vertex : Vertex,
    callback : VisitorCallback,
    visited = {} of Symbol => Bool,
    path = [] of Symbol
  )

    node = vertex.name
    incoming = vertex.incoming
    incoming_names = vertex.incoming_names

    return if visited[node]?

    path << node
    visited[node] = true

    incoming_names.each do |name|
      visit(incoming[name], callback, visited, path)
    end

    callback.call(vertex, path)
    path.pop
  end

  struct Vertex
    property :name, :incoming, :incoming_names, :has_outgoing, :value

    def initialize(
      *,
      @name : Symbol,
      @incoming = {} of Symbol => Vertex,
      @incoming_names = [] of Symbol,
      @has_outgoing = false,
    )
    end
  end

  struct Graph
    getter :verticies, :nodes
    def initialize(@nodes = [] of Symbol, @verticies = {} of Symbol => Vertex); end

    def add(node : Symbol) : Vertex
      if vertex = verticies[node]?
        return vertex
      end

      vertex = Vertex.new(name: node)
      verticies[node] = vertex
      nodes << node

      vertex
    end

    def add_edge(from : Symbol, to : Symbol)
      return if from == to

      from_vertex = add(from)
      to_vertex = add(to)

      return if to_vertex.incoming[from]?

      # visit
      Omnibus.visit(from_vertex, check_cycle(to))

      from_vertex.has_outgoing = true
      to_vertex.incoming[from] = from_vertex
      to_vertex.incoming_names << from
    end

    private def check_cycle(to : Symbol) : VisitorCallback
      -> (vertex : Vertex, path : Array(Symbol)) do
        raise "Bad Cycle #{ path.join(" <- ") }" if vertex.name == to
      end
    end
  end
end
