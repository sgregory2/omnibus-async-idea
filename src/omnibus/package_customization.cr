module Omnibus
  struct PackageCustomization
    getter :id

    def initialize(@id : Symbol); end
  end
end
