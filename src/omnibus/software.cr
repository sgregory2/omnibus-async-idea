require "./os/**"

module Omnibus
  struct Software
    include OS::Helper

    getter(
      :name,
      :dependencies,
      :build_git_revision,
      :build_iteration,
      :build_version,
      :description,
      :friendly_name,
      :homepage,
      :install_dir,
      :json_manifest_path,
      :license,
      :license_file,
      :license_file_path,
      :maintainer,
      :package_customization,
      :package_group,
      :package_name,
      :package_scripts_path,
      :package_user,
      :resources_path,
      :text_manifest_path,
      # lists
      :compressions,
      :config_files,
      :conflicts,
      :exclusions,
      :extra_package_locations,
      :replacements,
      :runtime_dependencies,
    )

    @build_git_revision : String?
    @build_iteration : Int32?
    @build_version : String?
    @description : String?
    @friendly_name : String?
    @homepage : String?
    @install_dir : String?
    @json_manifest_path : String?
    @license : String?
    @license_file : String?
    @license_file_path : String?
    @maintainer : String?
    @package_customization : PackageCustomization?
    @package_group : String?
    @package_name : String?
    @package_scripts_path : String?
    @package_user : String?
    @resources_path : String?
    @text_manifest_path : String?

    @dependencies : Array(Symbol)
    @builder : Builder?

    def initialize(@name : Symbol, @dependencies = [] of Symbol)
      @compressions = [] of Compression
      @config_files = [] of String
      @conflicts = [] of Symbol
      @exclusions = [] of Regex
      @extra_package_locations = [] of String
      @replacements = [] of Symbol
      @runtime_dependencies = [] of String
    end

    def build_git_revision(revision : String)
      @build_git_revision = revision
    end

    def build_iteration(iteration : Int32)
      @build_iteration = iteration
    end

    def build_version(version : String)
      @build_version = version
    end

    def compress(id : Symbol, &block : ->)
      compression = Compression.new(id)
      with compression yield
      @compressions << compression
    end

    def config_file(path : String)
      @config_files << path
    end

    def conflict(package : Symbol)
      @conflicts << package
    end

    def default_root
      raise NotImplementedError
    end

    def dependency(name : Symbol)
      dependencies << name
    end

    def description(val : String)
      @description = val
    end

    def exclude(pattern : Regexp)
      @exlusions << pattern
    end

    def extra_package_file(file_or_directory : String)
      @extra_package_locations << file_or_directory
    end

    def files_path
      raise NotImplementedError
    end

    def friendly_name(name : String)
      @friendly_name = name
    end

    # required
    def homepage(url : String)
      @homepage = url
    end

    # required
    def install_dir(path : String)
      @install_dir = path
    end

    def json_manifest_path(path : String)
      @json_manifest_path = path
    end

    def license(val : String)
      @license = val
    end

    def license_file(path : String)
      @license_file = path
    end

    def license_file_path(path : String)
      @license_file_path = path
    end

    # required
    def maintainer(name : String)
      @maintainer = name
    end

    def package(id : Symbol, &block)
      customization = PackageCustomization.new(id)
      with customization yield
      @package_customization = customization
    end

    def package_group(group : String)
      @package_group = group
    end

    def package_name(name : String)
      @package_name = name
    end

    def package_scripts_path(arg : String)
      @package_scripts_path = arg
    end

    def package_user(user : String)
      @package_user = user
    end

    def replace(name : Symbol)
      @replacements << name
    end

    def resources_path(path : String)
      @resources_path = path
    end

    def runtime_dependency(val : String)
      @runtime_dependencies << val
    end

    def text_manifest_path(path : String)
      @text_manifest_path = path
    end

    def build(&block : ->)
      builder = Builder.new
      with builder yield
      @builder = builder
    end

    def install_dir
      config.install_dir
    end

    def source_dir
      config.source_dir
    end

    def config
      Omnibus.config
    end

    protected def do_build(color)
      @builder.try &.build(name, color)
    end
  end
end

