require "file_utils"
require "./os/*"
require "colorize"

module Omnibus
  abstract class BaseCommand
    abstract def execute(name : Symbol, color : Symbol)
  end

  class Command < BaseCommand
    getter :command, :chdir, :env
    def initialize(@command : String, *, @chdir : String? = nil, @env : Hash(String, String)? = nil); end

    def execute(name, color)
      puts "#{name} > Running #{command}"
      output = IO::Memory.new
      error = IO::Memory.new
      p = Process.new(command, chdir: chdir, shell: true, env: env, output: output, error: error) #output: Process::Redirect::Pipe, error: Process::Redirect::Pipe)
      time = 0
      loop do
        log_output(name, output, error, color)

        break if !p.exists? || p.terminated?
        sleep 0.5
      end
      puts "Total time for #{command}: #{time}".colorize(:green)
    end

    def log_output(name, output, error, color)
      o = output.to_s
      e = error.to_s
      out = o.empty? ? "" : o.split(/\n/).map do |f|
        "#{name.colorize(color)}> #{f}"
      end.join("\n")
      err = e.empty? ? "" : e.split(/\n/).map do |f|
        "#{name.colorize(color)}> #{f.colorize(:red)}"
      end.join("\n")
      output.flush
      error.flush
      puts "#{out}" unless o.empty?
      puts "#{err}" unless e.empty?
    end
  end

  class Patch < BaseCommand
    getter :plevel, :patch_file, :chdir, :env
    # must be absolute paths
    def initialize(@patch_file : String, *, @plevel : Int32 = 1, @chdir : String? = nil, @env : Hash(String, String)? = nil); end

    def execute(name, color)
      cmd = "patch -p#{plevel} -i #{patch_file}"
      Command.new(cmd, chdir: chdir, env: env).execute(name, color)
    end
  end

  class Mkdir < BaseCommand
    getter :directory
    def initialize(@directory : String); end

    def execute(name, color)
      FileUtils.mkdir_p(directory)
    end
  end

  class Wget < BaseCommand
    getter :to_dir, :source_url, :unzip

    def initialize(@source_url : String, @to_dir : String = ".", *, @unzip : Bool = false); end

    def execute(name, color)
      commands = [
        "wget -O #{to_dir} #{source_url}",
      ]

      Command.new(commands.join(" && ")).execute(name, color)
    end
  end

  class TarXvf < BaseCommand
    getter :tar_file, :dest_dir

    def initialize(@tar_file : String, @dest_dir : String); end

    def execute(name, color)
      FileUtils.mkdir_p(dest_dir)
      Command.new("tar -xvf #{tar_file} -C #{dest_dir} --strip-components=1").execute(name, color)
    end
  end

  class Copy < BaseCommand
    getter :to, :from

    def initialize(@from : String, @to : String); end

    def execute(name, color)
      FileUtils.cp_r(from, to)
    end
  end

  struct Builder
    include OS::Helper

    def initialize
      @commandables = [] of BaseCommand | Proc(Nil)
    end

    def block(&block : ->)
      @commandables << block
    end

    def command(command : String, **opts)
      @commandables << Command.new(command, **opts)
    end

    def mkdir(path : String)
      @commandables << Mkdir.new(path)
    end

    def wget(from : String, to : String)
      @commandables << Wget.new(from, to)
    end

    def tar_xvf(tar_file : String, dest_dir : String)
      @commandables << TarXvf.new(tar_file, dest_dir)
    end

    def patch(patch : String, **args)
      @commandables << Patch.new(patch, **args)
    end

    def copy(from : String, to : String)
      @commandables << Copy.new(from, to)
    end

    def install_dir
      Omnibus.config.install_dir
    end

    def source_dir
      Omnibus.config.source_dir
    end

    protected def build(name, color)
      @commandables.each do |command|
        case command
        when Proc(Nil)
          command.call
        else
          command.execute(name, color)
        end
      end
    end
  end
end
