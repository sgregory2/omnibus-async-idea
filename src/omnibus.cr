require "./omnibus/**"
require "file_utils"

module Omnibus
  VERSION = "0.1.0"

  struct Config
    property(
      :base_dir,
      :source_dir,
      :install_dir,
      :build_dir
    )
    # TODO: don't hardcode platform behavior
    def initialize(
      *,
      @base_dir = "/var/cache/omnibus",
      @source_dir = "/var/opt/omnibus/source",
      @install_dir = "/var/opt/omnibus/install",
      @build_dir = "/var/opt/omnibus/build"
    )
    end
  end

  def self.registry
    @@registry ||= Registry.new
  end

  def self.configure(&block : ->)
    with config yield
  end

  def self.init
    [config.base_dir, config.source_dir, config.install_dir, config.build_dir].each do |folder|
      FileUtils.mkdir_p(folder)
    end

    FileUtils.mkdir_p("#{config.install_dir}/bin")
    FileUtils.mkdir_p("#{config.install_dir}/embedded/bin")
    FileUtils.mkdir_p("#{config.install_dir}/embedded/lib")
    FileUtils.mkdir_p("#{config.install_dir}/embedded/include")
  end

  @@config : Config?

  def self.config : Config
    @@config || Config.new
  end

  def self.software(name, &block)
    software = Software.new(name)
    with software yield

    registry << software
  end

  def self.build(things : Array(Symbol)? = nil)
    BuildOrchestrator.new(registry.dag, registry, things).build
  end
end
