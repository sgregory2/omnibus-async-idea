FROM crystallang/crystal as builder

WORKDIR /omnibus
COPY . /omnibus/.
RUN ls
RUN crystal build --release example/config/demo.cr

FROM ubuntu:latest
RUN apt update -y && apt install -y build-essential libevent-2.1-7
RUN apt install -y wget git libc6-dev cmake
RUN apt install -y pkg-config file gcc curl
RUN curl -sL https://deb.nodesource.com/setup_18.x | bash -
RUN apt install -y nodejs
RUN apt install -y autoconf bison libyaml-dev
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt-get -y install tzdata
RUN npm install --global yarn
COPY --from=builder /omnibus/demo demo

RUN mkdir /vendor

COPY --from=builder /omnibus/example/config/files /vendor/.
COPY --from=builder /omnibus/example/config/patches /patches/.





