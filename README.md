# omnibus idea

Build dependencies really fast.

Work in progress - proof of concept, builds are currently inaccurate.

![](doc/omnibus.gif)

## Run omnibus-gitlab example 

```crystal
git clone https://gitlab.com/sgregory2/omnibus-async-idea.git && cd omnibus-async-idea
docker build -t omnibus-async .
docker exec -it omnibus-async bash

> time ./demo
```

So far, compared to 22 minutes of work being done by omnibus-gitlab - this will build the same dependencies in ~7
         
```
real    6m41.369s
user    25m57.506s
sys     4m35.353s
```

## Usage example

```crystal
Omnibus.software :ruby do
   dependency :foo
end

Omnibus.software :foo do
   dependency :bar
   dependency :buz

   build do
      block do
         sleep 2
      end
   end
end

Omnibus.software :bar do
   dependency :owl

   build do
      block do
         puts "Longg bar"
         sleep 4
         puts "....still bar"
         sleep 4
         puts "...finishing"
      end
   end
end

Omnibus.software :owl do
   dependency :bob

   build do
      block do
         puts "start owl build"
         sleep 3
         puts "building owl....still"
         sleep 1
      end
   end
end

Omnibus.software :jack do
   dependency :bob

   build do
      block do
         sleep 1
         puts "JACK!"
      end
   end
end

Omnibus.software :buz do
end

Omnibus.software :bob do
   build do
      block do
         sleep 1
      end
   end
end


puts Omnibus.build
```
output
```shell
building buz
buz is built
building bob
bob is built
owl, jack is unblocked
building owl
start owl build
building jack
JACK!
jack is built
building owl....still
owl is built
bar is unblocked
building bar
Longg bar
....still bar
...finishing
bar is built
foo is unblocked
building foo
foo is built
ruby is unblocked
building ruby
ruby is built
```


## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     omnibus:
       github: your-github-user/omnibus
   ```

2. Run `shards install`

## Usage

```crystal
require "omnibus"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://github.com/your-github-user/omnibus/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [your-name-here](https://github.com/your-github-user) - creator and maintainer
