require "../harness"

Omnibus.software :unzip do
  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://downloads.sourceforge.net/project/infozip/UnZip%206.x%20%28latest%29/UnZip%206.0/unzip60.tar.gz"
    dest = "#{source_dir}/unzip"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "make -f unix/Makefile clean", env: env, chdir: dest
    command "make unix/Makefile generic", env: env, chdir: dest
    command "make -f unix/Makefile prefix=#{install_dir}/embedded install", env: env, chdir: dest
  end
end
