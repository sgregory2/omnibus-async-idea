require "../harness"

Omnibus.software :gpgme do
  dependency :libassuan
  dependency :gnupg
  dependency :zlib

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    env["CFLAGS"] += " -std=c99"

    # 1.14.0 is broken
    # https://dev.gnupg.org/T5561
    version = "1.18.0"
    url = "https://www.gnupg.org/ftp/gcrypt/gpgme/gpgme-#{version}.tar.bz2"
    dest = "#{source_dir}/gpgme"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-doc --disable-languages", env: env, chdir: dest
    command "make -j 3", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
