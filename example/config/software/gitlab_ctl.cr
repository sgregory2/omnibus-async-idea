require "../harness"

Omnibus.software :gitlab_ctl do
  dependency :omnibus_ctl

  build do
    mkdir "#{install_dir}/bin"
    foo = install_dir

    block do
      data = <<-EOF
#!/bin/bash

# Ensure the calling environment (disapproval look Bundler) does not infect our
# Ruby environment if gitlab-ctl is called from a Ruby script.
for ruby_env_var in RUBYOPT \\
                    RUBYLIB \\
                    BUNDLE_BIN_PATH \\
                    BUNDLE_GEMFILE \\
                    GEM_PATH \\
                    GEM_HOME
do
  unset $ruby_env_var
done

# This bumps the default svwait timeout from 7 seconds to 30 seconds
# As documented at http://smarden.org/runit/sv.8.html
export SVWAIT=30

if [ "$1" == "reconfigure" ] && [ "$UID" != "0" ]; then
  echo "This command must be executed as root user"
  exit 1
fi

#{foo}/embedded/bin/omnibus-ctl #{File.basename(foo)} '#{foo}/embedded/service/omnibus-ctl*' "$@"
EOF
      File.open("#{foo}/bin/gitlab-ctl", "w") do |f|
        f << data
      end
    end

    command "chmod 755 #{install_dir}/bin/gitlab-ctl"
    command "cp -Rf /vendor/gitlab-ctl-commands/* #{install_dir}/embedded/service/omnibus-ctl/."
  end
end
