require "../harness"

Omnibus.software :gitlab_exporter do
  dependency :ruby
  dependency :postgres

  build do
    version = "11.18.0"
    env = with_standard_compiler_flags(with_embedded_path)
    command "gem install gitlab-exporter --no-document --version #{version}", env: env
  end
end
