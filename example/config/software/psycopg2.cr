require "../harness"

Omnibus.software :psycopg2 do
  dependency :python3
  dependency :postgres_new

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/psycopg/psycopg2/archive/refs/tags/2_8_6.tar.gz"
    dest = "#{source_dir}/psycopg"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env["PATH"] = "#{install_dir}/embedded/postgresql/13/bin:#{env["PATH"]}"

    command "#{install_dir}/embedded/bin/python3 setup.py build_ext", env: env, chdir: dest
    command "#{install_dir}/embedded/bin/python3 setup.py install", env: env, chdir: dest
  end
end
