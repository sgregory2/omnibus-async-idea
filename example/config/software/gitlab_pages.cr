require "../harness"

Omnibus.software :gitlab_pages do
  build do
    url = "https://gitlab.com/gitlab-org/gitlab-pages/-/archive/v1.62.0/gitlab-pages-v1.62.0.tar.gz"
    dest = "#{source_dir}/gitlab_pages"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = { "GOPATH" => dest }

    command "make gitlab-pages", env: env, chdir: dest
    command "mv gitlab-pages #{install_dir}/embedded/bin/gitlab-pages", chdir: dest
  end
end
