require "../harness"

Omnibus.software :consul do
  build do
    url = "https://github.com/hashicorp/consul/archive/refs/tags/v1.12.2.tar.gz"
    dest = "#{source_dir}/consul"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {} of String => String
    env["GOPATH"] = dest
    env["PATH"] = "#{ENV["PATH"]}:#{env["GOPATH"]}/bin"

    command "make dev", env: env, chdir: dest
    mkdir "#{install_dir}/embedded/bin"
    command "cp bin/consul #{install_dir}/embedded/bin/.", chdir: dest
  end
end
