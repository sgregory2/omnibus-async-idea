require "../harness"

Omnibus.software :ncurses do
  build do
    download_dir = "#{source_dir}/ncurses"

    wget "https://ftp.gnu.org/gnu/ncurses/ncurses-6.3.tar.gz", "#{source_dir}/ncurses-6.3.tar.gz"
    tar_xvf "#{source_dir}/ncurses-6.3.tar.gz", download_dir

    env = with_standard_compiler_flags(with_embedded_path)
    env.delete("CPPFLAGS")
    env["CFLAGS"] = "-I/var/opt/omnibus/embedded/include #{env["CFLAGS"]?}"

    configure_command = String.build do |io|
      io << "./configure "
      io << "--prefix=#{install_dir}/embedded "
      io << "--enable-overwrite "
      io << "--with-shared "
      io << "--with-termlib "
      io << "--without-ada "
      io << "--without-cxx-binding "
      io << "--without-debug "
      io << "--without-manpages"
    end

    command configure_command, env: env, chdir: download_dir
    command "make -j 33", env: env, chdir: download_dir
    command "make install", env: env, chdir: download_dir

    # # huh?
    # command "make distclean", env: env, chdir: download_dir
    #
    # command "#{configure_command} --enable-widec", env: env, chdir: download_dir
    # command "make", env: env, chdir: download_dir
    # command "make installls", env: env, chdir: download_dir
  end
end
