require "../harness"

# skip for now
Omnibus.software :libiconv do
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    version = "1.15"
    url = "https://ftp.gnu.org/pub/gnu/libiconv/libiconv-#{version}.tar.gz"
    dest = "#{source_dir}/libiconv"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure", env: env, chdir: dest
    command "make -j 3", env: env, chdir: dest
    command "make install-lib libdir=#{install_dir}/embedded/lib includedir=#{install_dir}/embedded/include", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
