require "../harness"

Omnibus.software :gitlab_config_template do
  build do
    mkdir "#{install_dir}/embedded/gitlab-config-template"
    command "cp -Rf /vendor/gitlab-config-template/* #{install_dir}/embedded/gitlab-config-template/."
  end
end
