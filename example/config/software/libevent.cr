require "../harness"

Omnibus.software :libevent do
  dependency :libtool
  dependency :openssl

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/libevent/libevent/releases/download/release-2.1.12-stable/libevent-2.1.12-stable.tar.gz"
    dest = "#{source_dir}/libevent"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "/autogen.sh", env: env, chdir: dest
    command "./configure --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
