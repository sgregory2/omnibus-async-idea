require "../harness"

Omnibus.software :postgres_new do
  dependency :zlib
  dependency :openssl
  dependency :libedit
  dependency :ncurses
  dependency :libossp_uuid
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "13.6"
    major_version = "14"
    libpq = "libpq.so.5"

    url = "https://ftp.postgresql.org/pub/source/v#{version}/postgresql-#{version}.tar.bz2"
    dest = "#{source_dir}/postgres-new"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    env["CFLAGS"] += " -fno-omit-frame-pointer"
    prefix = "#{install_dir}/embedded/postgresql/#{major_version}"


    command "./configure --prefix=#{prefix} --with-libedit-preferred --with-openssl --with-uuid-ossp", env: env, chdir: dest
    command "make world -j 4", env: env, chdir: dest
    command "make install-world", env: env, chdir: dest
    command "ln -s #{prefix}/lib/#{libpq} #{install_dir}/embedded/lib/#{libpq}"

    foo = install_dir

    block do
      Dir.glob("#{prefix}/bin/*").each do |bin_file|
        Process.run("ln -s #{bin_file} #{foo}/embedded/bin/#{File.basename(bin_file)}", shell: true, env: env, chdir: dest, output: STDOUT, error: STDOUT)
      end
    end
  end
end
