require "../harness"

Omnibus.software :go_crond do
  build do
    url = "https://github.com/webdevops/go-crond/archive/refs/tags/21.5.0.tar.gz"
    dest = "#{source_dir}/go_crond"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
     "GOPATH" => dest,
    }

    command "make build-local", env: env, chdir: dest
    mkdir "#{install_dir}/embedded/bin"
    command "cp go-crond #{install_dir}/embedded/bin", chdir: dest
  end
end
