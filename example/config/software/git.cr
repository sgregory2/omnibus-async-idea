require "../harness"

Omnibus.software :git do
  dependency :zlib
  dependency :openssl
  dependency :curl
  dependency :pcre2
  dependency :libiconv

  build do
    url = "https://gitlab.com/gitlab-org/git/-/archive/v2.37.2/git-v2.37.2.tar.gz"
    dest = "#{source_dir}/git"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags(with_embedded_path)
    git_cflags = "-fno-omit-frame-pointer"

    build_options = [
      "# Added by Omnibus git software definition git.rb",
      "GIT_APPEND_BUILD_OPTIONS += CURLDIR=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += ICONVDIR=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += ZLIB_PATH=#{install_dir}/embedded",
      "GIT_APPEND_BUILD_OPTIONS += NEEDS_LIBICONV=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += NO_R_TO_GCC_LINKER=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += INSTALL_SYMLINKS=YesPlease",
      "GIT_APPEND_BUILD_OPTIONS += CFLAGS=\"#{git_cflags}\"",
      "GIT_APPEND_BUILD_OPTIONS += OPENSSLDIR=#{install_dir}/embedded"
    ]

    block do
      File.open(File.join(dest, "config.make"), "a") do |file|
        file.print build_options.join("\n")
      end
    end

    command "make git install-bundled-git PREFIX=#{install_dir}/embedded GIT_PREFIX=#{install_dir}/embedded", env: env, chdir: dest
  end
end
