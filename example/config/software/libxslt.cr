require "../harness"

Omnibus.software :libxslt do
  dependency :libxml2
  dependency :liblzma
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    version = "1.1.35"
    source_url = "https://download.gnome.org/sources/libxslt/1.1/libxslt-#{version}.tar.xz"
    dest = "#{source_dir}/libxslt"

    wget source_url, "#{dest}.tar.xz"
    tar_xvf "#{dest}.tar.xz", dest

    command "./configure --with-libxml-prefix=#{install_dir}/embedded --without-python --without-crypto", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install" , env: env, chdir: dest
  end
end
