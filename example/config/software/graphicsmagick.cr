require "../harness"

Omnibus.software :graphicsmagick do
  dependency :libpng
  dependency :libjpeg_turbo
  dependency :libtiff
  dependency :zlib

  build do
    version = "1.3.36"
    url = "https://sourceforge.net/projects/graphicsmagick/files/graphicsmagick/#{version}/GraphicsMagick-#{version}.tar.gz"
    dest = "#{source_dir}/graphicsmagick"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags(with_embedded_path)

    configure_command = [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-openmp",
      "--without-magick-plus-plus",
      "--with-perl=no",
      "--without-bzlib",
      "--without-dps",
      "--without-fpx",
      "--without-gslib",
      "--without-jbig",
      "--without-webp",
      "--without-jp2",
      "--without-lcms2",
      "--without-trio",
      "--without-ttf",
      "--without-umem",
      "--without-wmf",
      "--without-xml",
      "--without-x",
      "--with-tiff=yes",
      "--with-lzma=yes",
      "--with-jpeg=yes",
      "--with-zlib=yes",
      "--with-png=yes",
      "--with-sysroot=#{install_dir}/embedded"
    ]

    command configure_command.join(" "), env: env, chdir: dest
    command "make -j 14", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
