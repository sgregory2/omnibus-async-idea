require "../harness"

Omnibus.software :grafana_dashboards do
  dependency :grafana

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    url = "https://gitlab.com/gitlab-org/grafana-dashboards/-/archive/v1.9.0/grafana-dashboards-v1.9.0.tar.gz"
    dest = "#{source_dir}/grafana_dashboards"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    mkdir "#{install_dir}/embedded/service/grafana-dashboards"
    command "cp -Rf omnbius/* #{install_dir}/embedded/service/grafana-dashboards/.", chdir: dest
  end
end
