require "../harness"

Omnibus.software :runit do
  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "2.1.2"
    url = "http://smarden.org/runit/runit-#{version}.tar.gz"
    dest = "#{source_dir}/runit"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "sed -i -e \"s/^char\ \*varservice\ \=\"\/service\/\";$/char\ \*varservice\ \=\"' + install_dir.gsub('/', '\\/') + '\/service\/\";/\" sv.c", env: env, chdir: dest

    # TODO: the following is not idempotent
    command "sed -i -e s:-static:: Makefile", env: env, chdir: dest

    command "make", env: env, chdir: dest
    command "make check", env: env, chdir: dest

    # Move it
    mkdir "#{install_dir}/embedded/bin"
    command "cp #{dest}/* #{install_dir}/embedded/bin/.", chdir: dest
  end
end
