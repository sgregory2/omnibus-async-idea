require "../harness"

Omnibus.software :pcre2 do
  dependency :libedit
  dependency :ncurses
  dependency :config_guess
  dependency :libtool

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://osdn.net/projects/sfnet_pcre/downloads/pcre2/10.37/pcre2-10.37.tar.gz"
    dest = "#{source_dir}/pcre"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./autogen.sh", env: env, chdir: dest

    config_cmd = String.build do |io|
      io << "./configure --prefix=#{install_dir}/embedded "
      io << "--disable-cpp --enable-utf --enable-unicode-properties "
      io << "--enable-jit --enable-pcretest-libedit"
    end

    command config_cmd, env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
