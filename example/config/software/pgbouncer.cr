require "../harness"

Omnibus.software :pgbouncer do
  dependency :openssl
  dependency :libevent

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.12.0"
    url = "https://www.pgbouncer.org/downloads/files/#{version}/pgbouncer-#{version}.tar.gz"
    dest = "#{source_dir}/pgbouncer"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    prefix = "#{install_dir}/embedded"

    configure_command = ["./configure", "--prefix=#{prefix}", "--with-libevent=#{prefix}", "--with-openssl=#{prefix}"]

    command configure_command.join(" "), env: env, chdir: dest
    command "make -j 7", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
