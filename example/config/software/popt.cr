require "../harness"

Omnibus.software :popt do
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.16"
    url = "https://ftp.osuosl.org/pub/blfs/conglomeration/popt/popt-#{version}.tar.gz"
    dest = "#{source_dir}/popt"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-nls", env: env, chdir: dest
    command "make -j 3", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
