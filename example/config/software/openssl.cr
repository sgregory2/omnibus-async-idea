require "../harness"

Omnibus.software :openssl do
  dependency :cacerts

  build do
    download_url = "https://ftp.openssl.org/source/old/1.1.1/openssl-1.1.1l.tar.gz"
    dest_dir = "#{source_dir}/openssl"

    wget download_url, "#{dest_dir}.tar.gz"
    tar_xvf "#{dest_dir}.tar.gz", dest_dir

    env = with_standard_compiler_flags(with_embedded_path)

    cmd = String.build do |io|
      io << "./config disable-gost "
      io << "--prefix=#{install_dir}/embedded "
      io << "no-comp "
      io << "no-idea no-mdc2 no-rc5 no-ssl2 no-ssl3 no-zlib shared "
      io << env["CFLAGS"]
      io << " "
      io << env["LDFLAGS"]
    end
    command cmd, env: env, chdir: dest_dir
    command "make depend", env: env, chdir: dest_dir
    command "make -j 33", env: env, chdir: dest_dir

    command "make install_sw", env: env, chdir: dest_dir
    command "make install_ssldirs", env: env, chdir: dest_dir
  end
end
