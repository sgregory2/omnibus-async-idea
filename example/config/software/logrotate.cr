require "../harness"

Omnibus.software :logrotate do
  dependency :popt

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/logrotate/logrotate/archive/refs/tags/3.18.0.tar.gz"
    dest = "#{source_dir}/logrotate"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./autogen.sh", env: env, chdir: dest
    command "./configure --prefix=#{install_dir}/embedded --without-selinux", env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest

    command "make install", env: env, chdir: dest
  end
end
