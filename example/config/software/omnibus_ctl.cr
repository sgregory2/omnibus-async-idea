require "../harness"

Omnibus.software :omnibus_ctl do
  dependency :bundler

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    dest = "#{source_dir}/omnibus-ctl"

    command "git clone https://github.com/chef/omnibus-ctl.git #{dest}"
    command "gem build omnibus-ctl.gemspec", env: env, chdir: dest
    command "gem install omnibus-ctl-*.gem --no-document", env: env, chdir: dest
  end
end
