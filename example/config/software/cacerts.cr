require "../harness"

Omnibus.software :cacerts do
  version = "2022.07.19"
  url_version = version.gsub(".", "-")
  source_url = "https://curl.haxx.se/ca/cacert-#{url_version}.pem"

  build do
    wget source_url, "#{source_dir}/cacert-#{url_version}.pem"
    mkdir "#{install_dir}/embedded/ssl/certs"
    copy "#{source_dir}/cacert-#{url_version}.pem", "#{install_dir}/embedded/ssl/certs/cacert.pem"
    command "ln -s #{install_dir}/embedded/ssl/certs/cacert.pem #{install_dir}/embedded/ssl/cert.pem"

    foo = install_dir

    block do
      File.chmod("#{foo}/embedded/ssl/certs/cacert.pem", 644)
    end
  end
end
