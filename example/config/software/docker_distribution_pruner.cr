require "../harness"

Omnibus.software :docker_distribution_pruner do
  build do
    version = "0.2.0"
    url = "https://gitlab.com/gitlab-org/docker-distribution-pruner/-/archive/v0.2.0/docker-distribution-pruner-v0.2.0.tar.gz"

    dest = "#{source_dir}/docker_distribution_pruner"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = { "GOPATH" => dest }

    command "go build -ldflags '-s -w' ./cmds/docker-distribution-pruner", env: env, chdir: dest
    command "cp docker-distribution-pruner #{install_dir}/embedded/bin/.", chdir: dest
  end
end
