require "../harness"

Omnibus.software :krb5 do
  dependency :openssl

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/krb5/krb5/archive/refs/tags/krb5-1.17.2-final.tar.gz"
    dest = "#{source_dir}/krb5"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    mkdir "#{install_dir}/embedded/sbin"

    command "mv src/configure.in src/configure.ac", env: env, chdir: dest
    patch "/patches/krb5/Add-option-to-build-without-libkeyutils.patch", chdir: dest
    command "mv src/configure.ac src/configure.in", env: env, chdir: dest

    command "autoreconf", env: env, chdir: "#{dest}/src"
    command "./configure --prefix=#{install_dir}/embedded --without-system-verto --without-keyutils --disable-pkinit", env: env, chdir: "#{dest}/src"
    command "make -j 4", env: env, chdir: "#{dest}/src"
    command "make install", env: env, chdir: "#{dest}/src"
  end
end
