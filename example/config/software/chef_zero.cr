require "../harness"

Omnibus.software :chef_zero do
  dependency :ruby

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    version = "15.0.11"

    cmd = String.build do |io|
      io << "gem install chef-zero --clear-sources -s https://packagecloud.io/cinc-project/stable -s https://rubygems.org "
      io << "--version '#{version}' "
      io << "--bindir '#{install_dir}/embedded/bin' "
      io << "--no-document"
    end

    command cmd, env: env
  end
end
