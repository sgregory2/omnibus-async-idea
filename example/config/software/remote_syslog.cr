require "../harness"

Omnibus.software :remote_syslog do
  dependency :ruby

  build do
    version = "1.6.15"
    env = with_standard_compiler_flags(with_embedded_path)
    command "gem install remote_syslog -n #{install_dir}/embedded/bin --no-document -v #{version}", env: env
  end
end
