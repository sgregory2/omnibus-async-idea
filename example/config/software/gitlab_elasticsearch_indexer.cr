require "../harness"

Omnibus.software :gitlab_elasticsearch_indexer do
  dependency :libicu

  build do
    url = "https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/archive/v2.16.0/gitlab-elasticsearch-indexer-v2.16.0.tar.gz"
    dest = "#{source_dir}/gitlab_elasticsearch_indexer"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags(with_embedded_path)
    command "make install PREFIX=#{install_dir}/embedded", env: env, chdir: dest
  end
end
