require "../harness"

Omnibus.software :zlib do
  build do
    url = "https://github.com/madler/zlib/archive/refs/tags/v1.2.12.tar.gz"
    dest = "#{source_dir}/zlib"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags
    env["CFLAGS"] += " -O3"
    env["CFLAGS"] += " -fno-omit-frame-pointer"

    command "./configure --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
