require "../harness"

Omnibus.software :gitlab_rails_bundle do
  dependency :gitlab_rails

  build do
    dest = "#{source_dir}/gitlab_rails"

    env = with_standard_compiler_flags(with_embedded_path)
    bundle_without = %w(development test mysql)

    command "bundle config build.gpgme --use-system-libraries", env: env, chdir: dest
    command "bundle config build.nokogiri --use-system-libraries --with-xml2-include=#{install_dir}/embedded/include/libxml2 --with-xslt-include=#{install_dir}/embedded/include/libxslt", env: env, chdir: dest
    command "bundle config set --local frozen 'true'", env: env, chdir: dest
    command "bundle config --global jobs 9", env: env, chdir: dest
    command "bundle install --without #{bundle_without.join(" ")} --jobs 5 --retry 5", env: env, chdir: dest
  end
end
