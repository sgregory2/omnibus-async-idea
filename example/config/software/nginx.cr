require "../harness"

Omnibus.software :nginx do
  dependency :pcre
  dependency :zlib
  dependency :openssl
  # huh? these don"t exist
  dependency :nginx_module_vts
  dependency :ngx_security_headers

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "http://nginx.org/download/nginx-1.20.2.tar.gz"
    dest = "#{source_dir}/nginx"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command [
      "./auto/configure",
      "--prefix=#{install_dir}/embedded",
      "--with-http_ssl_module",
      "--with-http_stub_status_module",
      "--with-http_gzip_static_module",
      "--with-http_v2_module",
      "--with-http_realip_module",
      "--with-http_sub_module",
      "--with-ipv6",
      "--with-debug",
      "--add-module=#{source_dir}/nginx_module_vts",
      "--add-module=#{source_dir}/ngx_security_headers",
      "--with-ld-opt=-L#{install_dir}/embedded/lib",
      "--with-cc-opt=\"-L#{install_dir}/embedded/lib -I#{install_dir}/embedded/include\""
    ].join(" "), env: env, chdir: dest

    command "make -j 5", env: { "LD_RUN_PATH" => "#{install_dir}/embedded/lib" }, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
