require "../harness"

Omnibus.software :gitlab_healthcheck do
  build do
    foo = install_dir
    block do
      File.open("#{foo}/bin/gitlab-healthcheck", "w") do |file|
        file.print <<-EOH
        #!/bin/sh

        error_echo()
        {
          echo "$1" 2>& 1
        }

        gitlab_healthcheck_rc='/opt/gitlab/etc/gitlab-healthcheck-rc'


        if ! [ -f ${gitlab_healthcheck_rc} ] ; then
          exit 1
        fi

        . ${gitlab_healthcheck_rc}

        exec /opt/gitlab/embedded/bin/curl $@ ${flags} ${url}
      EOH
      end
    end

    command "chmod 755 #{install_dir}/bin/gitlab-healthcheck"
  end
end
