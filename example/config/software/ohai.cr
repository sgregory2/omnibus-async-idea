require "../harness"

Omnibus.software :ohai do
  dependency :ruby

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "17.9.0"

    command "gem install ohai --version '#{version}' --bindir '#{install_dir}/embedded/bin' --no-document", env: env
  end
end
