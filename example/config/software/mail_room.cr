require "../harness"

Omnibus.software :mail_room do
  dependency :ruby

  build do
    version = "0.0.20"

    env = with_standard_compiler_flags(with_embedded_path)
    command "gem install gitlab-mail_room --no-document --version #{version}", env: env
  end
end
