require "../harness"

Omnibus.software :libxml2 do
  dependency :zlib
  dependency :libiconv
  dependency :liblzma
  dependency :config_guess

  build do
    version = "2.9.14"
    source_url = "https://download.gnome.org/sources/libxml2/2.9/libxml2-#{version}.tar.xz"
    env = with_standard_compiler_flags(with_embedded_path)
    dest = "#{source_dir}/libxml2"

    wget source_url, "#{source_dir}/libxml2.tar.xz"
    tar_xvf "#{source_dir}/libxml2.tar.xz", dest

    configure_cmd = String.build do |io|
      io << "./configure "
      io << "--prefix=#{install_dir}/embedded "
      io << "--with-zlib=#{install_dir}/embedded "
      io << "--with-iconv=#{install_dir}/embedded "
      io << "--with-lzma=#{install_dir}/embedded "
      io << "--with-sax1 "
      io << "--without-python "
      io << "--without-icu "
    end

    command configure_cmd, env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
