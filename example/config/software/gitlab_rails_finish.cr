require "../harness"

Omnibus.software :gitlab_rails_finish do
  dependency :gitlab_rails_text_compile
  dependency :gitlab_rails_assets_compile

  build do
    dest = "#{source_dir}/gitlab_rails"
    mkdir "#{install_dir}/embedded/service/gitlab-rails"
    command "cp -Rf ./* #{install_dir}/embedded/service/gitlab-rails/.", chdir: dest
  end
end
