require "../harness"

Omnibus.software :bzip2 do
  dependency :zlib
  dependency :openssl

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    env["CFLAGS"] += " -fPIC"

    version = "1.0.8"
    url = "https://sourceware.org/pub/bzip2/bzip2-#{version}.tar.gz"
    dest = "#{source_dir}/bzip2"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    patch "/patches/bzip2/makefile_take_env_vars.patch", chdir: dest

    # huh?
    command "make PREFIX='#{install_dir}/embedded' VERSION='#{version}'", env: env, chdir: dest
    command "make PREFIX='#{install_dir}/embedded' VERSION='#{version}' -f Makefile-libbz2_so", env: env, chdir: dest
    command "make PREFIX='#{install_dir}/embedded' VERSION='#{version}' install", env: env, chdir: dest
  end
end
