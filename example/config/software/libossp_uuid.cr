require "../harness"

Omnibus.software :libossp_uuid do
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.6.2"
    url = "https://www.mirrorservice.org/sites/ftp.ossp.org/pkg/lib/uuid/uuid-#{version}.tar.gz"
    dest = "#{source_dir}/uuid"

    wget url, "#{source_dir}/uuid.tar.gz"
    tar_xvf "#{source_dir}/uuid.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
