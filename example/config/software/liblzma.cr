require "../harness"

Omnibus.software :liblzma do
  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "5.2.4"
    url = "http://tukaani.org/xz/xz-#{version}.tar.gz"
    dest = "#{source_dir}/xz"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure --disable-debug --disable-dependency-tracking  --disable-doc --disable-scripts", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
