require "../harness"

Omnibus.software :gitlab_rails_workhorse do
  dependency :gitlab_rails

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    dest = "#{source_dir}/gitlab_rails/workhorse"
    command "make install -C workhorse PREFIX=#{install_dir}/embedded", chdir: dest
  end
end
