require "../harness"

Omnibus.software :chef_acme do
  dependency :gitlab_cookbooks
  dependency :acme_client

  build do
    version = "v4.1.5"
    url = "https://github.com/schubergphilis/chef-acme/archive/refs/tags/v4.1.5.tar.gz"
    dest = "#{source_dir}/chef-acme"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", "#{install_dir}/embedded/cookbooks/acme"
  end
end
