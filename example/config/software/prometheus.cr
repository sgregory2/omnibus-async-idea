require "../harness"
require "../prometheus_flags"

Omnibus.software :prometheus do
  build do
    version = "2.33.4"
    url = "https://github.com/prometheus/prometheus/archive/refs/tags/v2.33.4.tar.gz"
    dest = "#{source_dir}/prometheus"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "GO111MODULE" => "on",
    }

    flags = PrometheusFlags.new

    command "go build -tags netgo,builtinassets -ldflags '#{flags.ldflags(version)}' ./cmd/prometheus", env: env, chdir: dest
    command "cp prometheus #{install_dir}/embedded/bin/.", chdir: dest
  end
end
