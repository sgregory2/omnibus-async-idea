require "../harness"

Omnibus.software :python_docutils do
  dependency :python3

  build do
    version = "0.16"
    env = with_standard_compiler_flags(with_embedded_path)
    command "#{install_dir}/embedded/bin/pip3 install --compile docutils==#{version}", env: env
  end
end
