require "../harness"

Omnibus.software :libjpeg_turbo do
  dependency :zlib

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/libjpeg-turbo/libjpeg-turbo/archive/refs/tags/2.1.2.tar.gz"
    dest = "#{source_dir}/libjpeg_turbo"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "cmake -G\"Unix Makefiles\" -DCMAKE_INSTALL_LIBDIR:PATH=lib -DCMAKE_INSTALL_PREFIX=#{install_dir}/embedded", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
