require "../harness"

Omnibus.software :rsync do
  dependency :popt

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "3.1.3"
    url = "https://rsync.samba.org/ftp/rsync/src/rsync-#{version}.tar.gz"
    dest = "#{source_dir}/rsync"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-iconv", chdir: dest, env: env
    command "make -j 7", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
