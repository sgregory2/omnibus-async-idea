require "../harness"

Omnibus.software :pgbouncer_exporter do
  build do
    url = "https://github.com/prometheus-community/pgbouncer_exporter/archive/refs/tags/v0.4.0.tar.gz"

    dest = "#{source_dir}/pgbounce_exporter"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "GO111MODULE" => "on"
    }
    version = "0.4.0"
    common_version = "github.com/prometheus/common/version"

    ldflags = [
      "-X #{common_version}.Version=#{version}",
      "-X #{common_version}.Branch=master",
      "-X #{common_version}.BuildUser=GitLab-Omnibus",
      "-s",
      "-w"
    ]

    command "go build -mod=vendor -ldflags '#{ldflags.join(" ")}'", env: env, chdir: dest
    command "cp pgbouncer_exporter #{install_dir}/embedded/bin/.", env: env, chdir: dest
  end
end
