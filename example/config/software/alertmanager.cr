require "../harness"

Omnibus.software :alertmanager do
  build do
    version = "0.23.0"
    url = "https://github.com/prometheus/alertmanager/archive/refs/tags/v0.23.0.tar.gz"
    dest = "#{source_dir}/alertmanager"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "GO111MODULE" => "on",
    }
    common_version = "github.com/prometheus/common/version"

    ldflags = [
      "-X #{common_version}.Version=#{version}",
      "-X #{common_version}.Branch=master",
      "-X #{common_version}.BuildUser=GitLab-Omnibus",
      "-s",
      "-w"
    ]

    command "go build -ldflags '#{ldflags.join(" ")}' ./cmd/alertmanager", env: env, chdir: dest
    command "cp alertmanager #{install_dir}/embedded/bin/.", chdir: dest
  end
end
