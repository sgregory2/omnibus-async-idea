require "../harness"

Omnibus.software :gitlab_rails_text_compile do
  dependency :gitlab_rails_bundle
  dependency :gitlab_rails_yarn

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    assets_compile_env = {
      "NODE_ENV" => "production",
      "RAILS_ENV" => "production",
      "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
      "USE_DB" => "false",
      "SKIP_STORAGE_VALIDATION" => "true",
      "NODE_OPTIONS" => "--max_old_space_size=3584"
    }
    dest = "#{source_dir}/gitlab_rails"
    command "bundle exec rake gettext:compile", env: assets_compile_env, chdir: dest
  end
end
