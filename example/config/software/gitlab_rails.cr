require "../harness"
require "yaml"

Omnibus.software :gitlab_rails do
  dependency :pkg_config_lite
  dependency :ruby
  dependency :bundler
  dependency :libxml2
  dependency :libxslt
  dependency :curl
  dependency :rsync
  dependency :libicu
  dependency :postgres
  dependency :postgres_new
  dependency :python_docutils
  dependency :krb5
  dependency :registry
  dependency :unzip
  dependency :libre2
  dependency :gpgme
  dependency :graphicsmagick
  dependency :exiftool

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    url = "https://gitlab.com/gitlab-org/gitlab-foss/-/archive/v15.1.5/gitlab-foss-v15.1.5.tar.gz"

    dest = "#{source_dir}/gitlab_rails"
    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    # In order to compile the assets, we need to get to a state where rake can
    # load the Rails environment.
    command "cp config/gitlab.yml.example config/gitlab.yml", env: env, chdir: dest
    command "cp config/secrets.yml.example config/secrets.yml", env: env, chdir: dest

    block do
      yaml = File.open("#{dest}/config/database.yml.postgresql") do |file|
        YAML.parse(file)
      end

      yaml.as_h.each do |_key, databases|
        databases.as_h.delete("geo")
      end

      File.open("#{dest}/config/database.yml", "w") do |file|
        yaml.to_yaml(file)
      end
    end
  end
end
