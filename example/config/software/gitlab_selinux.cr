require "../harness"

Omnibus.software :gitlab_selinux do
  build do
    mkdir "#{install_dir}/embedded/selinux"
    command "cp -Rf /vendor/gitlab-selinux/* #{install_dir}/embedded/selinux/."
  end
end
