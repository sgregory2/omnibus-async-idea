require "../harness"

Omnibus.software :registry do
  build do
    url = "https://gitlab.com/gitlab-org/container-registry/-/archive/v3.57.0-gitlab/container-registry-v3.57.0-gitlab.tar.gz"
    dest = "#{source_dir}/registry"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "BUILDTAGS" => "include_gcs include_oss"
    }

    command "make build", env: env, chdir: dest
    command "make binaries", env: env, chdir: dest
    command "mv #{dest}/bin/* #{install_dir}/embedded/bin"
  end
end
