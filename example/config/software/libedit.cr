require "../harness"

Omnibus.software :libedit do
  dependency :ncurses
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "20120601-3.0"
    url = "http://www.thrysoee.dk/editline/libedit-#{version}.tar.gz"
    dest = "#{source_dir}/libedit"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure  --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make -j", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
