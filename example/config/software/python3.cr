require "../harness"

Omnibus.software :python3 do
  dependency :libedit
  dependency :ncurses
  dependency :zlib
  dependency :openssl
  dependency :bzip2
  dependency :libffi
  dependency :liblzma
  dependency :libyaml

  build do
    lib_path = ["#{install_dir}/embedded/lib", "#{install_dir}/embedded/lib64", "#{install_dir}/lib", "#{install_dir}/lib64", "#{install_dir}/libexec"]

    env = {
      "CFLAGS" => "-I#{install_dir}/embedded/include -O3 -g -pipe",
      "LDFLAGS" => "-Wl,-rpath,#{lib_path.join(",-rpath,")} -L#{lib_path.join(" -L")} -I#{install_dir}/embedded/include"
    }

    version = "3.9.6"
    url = "https://www.python.org/ftp/python/#{version}/Python-#{version}.tgz"
    dest = "#{source_dir}/python"

    wget url, "#{dest}.tgz"
    tar_xvf "#{dest}.tgz", dest

    command "./configure --prefix=#{install_dir}/embedded --enable-shared --with-readline=editline --with-dbmliborder=", env: env, chdir: dest
    command "make -j 12", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
