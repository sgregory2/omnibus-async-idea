require "../harness"

Omnibus.software :grafana do
  build do
    arch = "amd64"
    version = "7.5.16"
    url = "https://dl.grafana.com/oss/release/grafana-#{version}.linux-#{arch}.tar.gz"
    dest = "#{source_dir}/grafana"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "cp bin/grafana-server #{install_dir}/embedded/bin/grafana-server", chdir: dest
    command "cp bin/grafana-cli #{install_dir}/embedded/bin/grafana-cli", chdir: dest
    # Static assets.

    mkdir "#{install_dir}/embedded/service/grafana"
    command "cp public #{install_dir}/embedded/service/grafana/public", chdir: dest

    # Default configuration.
    mkdir "#{install_dir}/embedded/service/grafana/conf"
    command "cp conf/defaults.ini #{install_dir}/embedded/service/grafana/conf/defaults.ini", chdir: dest
  end
end
