require "../harness"

Omnibus.software :libgpg_error do
  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.39"
    url = "https://www.gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-#{version}.tar.bz2"
    dest = "#{source_dir}/libgpg-error"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-doc", env: env, chdir: dest
    command "make -j 3", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
