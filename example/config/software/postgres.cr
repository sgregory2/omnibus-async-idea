require "../harness"

Omnibus.software :postgres do
  dependency :zlib
  dependency :openssl
  dependency :libedit
  dependency :ncurses
  dependency :libossp_uuid
  dependency :config_guess

  build do
    major_version = "12"
    env = with_standard_compiler_flags(with_embedded_path)
    env["CFLAGS"] += " -fno-omit-frame-pointer"
    prefix = "#{install_dir}/embedded/postgresql/#{major_version}"

    version = "12.10"
    url = "https://ftp.postgresql.org/pub/source/v#{version}/postgresql-#{version}.tar.bz2"
    dest = "#{source_dir}/postgres"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    command "./configure --prefix=#{prefix} --with-libedit-preferred --with-openssl --with-uuid-ossp", env: env, chdir: dest
    command "make world -j 4", env: env, chdir: dest
    command "make install-world", env: env, chdir: dest
  end
end
