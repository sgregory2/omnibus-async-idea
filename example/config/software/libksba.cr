require "../harness"

Omnibus.software :libksba do
  dependency :libgpg_error

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.4.0"
    url = "https://www.gnupg.org/ftp/gcrypt/libksba/libksba-#{version}.tar.bz2"
    dest = "#{source_dir}/libksba"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-doc", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
