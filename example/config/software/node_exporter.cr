require "../harness"
require "../prometheus_flags"

Omnibus.software :node_exporter do
  build do
    version = "1.3.1"
    url = "https://github.com/prometheus/node_exporter/archive/refs/tags/v1.3.1.tar.gz"

    dest = "#{source_dir}/node_exporter"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "CGO_ENABLED" => "0",
      "GO111MODULE" => "on"
    }

    flags = PrometheusFlags.new

    command "go build -ldflags '#{flags.ldflags(version)}'", env: env, chdir: dest
    command "cp node_exporter #{install_dir}/embedded/bin"
  end
end
