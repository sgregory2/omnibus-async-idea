require "../harness"

Omnibus.software :pcre do
  dependency :libedit
  dependency :ncurses
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "8.44"
    url = "http://downloads.sourceforge.net/project/pcre/pcre/#{version}/pcre-#{version}.tar.gz"
    dest = "#{source_dir}/pcre"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    config_cmd = String.build do |io|
      io << "./configure --prefix=#{install_dir}/embedded "
      io << "--disable-cpp --enable-utf --enable-unicode-properties "
      io << "--enable-pcretest-libedit"
    end

    command config_cmd, env: env, chdir: dest
    command "make -j 33", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
