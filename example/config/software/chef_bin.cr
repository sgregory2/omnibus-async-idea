require "../harness"

Omnibus.software :chef_bin do
  dependency :ruby

  build do
    version = "17.10.0"
    env = with_standard_compiler_flags(with_embedded_path)

    cmd = String.build do |io|
      io << "gem install chef-bin --clear-sources "
      io << "-s https://packagecloud.io/cinc-project/stable "
      io << "-s https://rubygems.org "
      io << "--version '#{version}' "
      io << "--bindir '#{install_dir}/embedded/bin' "
      io << "--no-document"
    end

    command cmd, env: env
  end
end
