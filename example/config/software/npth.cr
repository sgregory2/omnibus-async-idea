require "../harness"

Omnibus.software :npth do
  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "1.6"
    url = "https://www.gnupg.org/ftp/gcrypt/npth/npth-#{version}.tar.bz2"
    dest = "#{source_dir}/npth"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-doc", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
