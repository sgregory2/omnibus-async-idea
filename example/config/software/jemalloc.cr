require "../harness"

Omnibus.software :jemalloc do
  dependency :redis

  build do
    version = "5.3.0"
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://github.com/jemalloc/jemalloc/archive/refs/tags/5.3.0.tar.gz"
    dest = "#{source_dir}/jemalloc"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    autogen_command = [
      "./autogen.sh",
      "--enable-prof",
      "--prefix=#{install_dir}/embedded",
      "--with-lg-page=12"
    ]

    command autogen_command.join(" "), env: env, chdir: dest
    command "make -j 5 build_lib", env: env, chdir: dest
    command "make install_lib", env: env, chdir: dest
    command "make install_bin", env: env, chdir: dest
  end
end
