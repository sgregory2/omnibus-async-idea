require "../harness"

Omnibus.software :libtool do
  dependency :config_guess

  build do
    version = "2.4.6"
    env = with_standard_compiler_flags(with_embedded_path)

    source_url = "https://ftp.gnu.org/gnu/libtool/libtool-#{version}.tar.gz"
    dest = "#{source_dir}/libtool"

    wget source_url, "#{source_dir}/libtool.tar.gz"
    tar_xvf "#{source_dir}/libtool.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
