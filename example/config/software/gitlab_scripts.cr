require "../harness"

Omnibus.software :gitlab_scripts do
  build do
    mkdir "#{install_dir}/embedded/gitlab-scripts"
    command "cp -Rf /vendor/gitlab-scripts/* #{install_dir}/embedded/gitlab-scripts/."
  end
end
