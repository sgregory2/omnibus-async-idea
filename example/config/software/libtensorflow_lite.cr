require "../harness"
#  Skip this one - nothing depends on it
Omnibus.software :libtensorflow_lite do
  build do
    url = "https://github.com/tensorflow/tensorflow/archive/refs/tags/v2.5.0.tar.gz"
    dest = "#{source_dir}/tensorflow-zip"

    cwd = "#{source_dir}/tensorflow"
    mkdir cwd

    env = { "CC" => "/usr/bin/gcc-11", "CXX" => "/usr/bin/g++" }

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    # this is broken
    command "cmake #{dest}/tensorflow/lite/c", chdir: cwd, env: env
    command "cmake --build .", chdir: cwd, env: env
    command "mv tensorflowlite_c.* #{install_dir}/embedded/lib", chdir: cwd
  end
end
