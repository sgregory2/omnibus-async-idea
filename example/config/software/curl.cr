require "../harness"

Omnibus.software :curl do
  dependency :zlib
  dependency :openssl
  dependency :libtool

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    openssl_library_path = "#{install_dir}/embedded"
    url = "https://curl.se/download/curl-7.84.0.tar.gz"
    dest = "#{source_dir}/curl"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    configure_cmd = [
      "./configure",
      "--prefix=#{install_dir}/embedded",
      "--disable-option-checking",
      "--disable-manual",
      "--disable-debug",
      "--enable-optimize",
      "--disable-ldap",
      "--disable-ldaps",
      "--disable-rtsp",
      "--enable-proxy",
      "--disable-pop3",
      "--disable-imap",
      "--disable-smtp",
      "--disable-gopher",
      "--disable-dependency-tracking",
      "--enable-ipv6",
      "--without-libidn2",
      "--without-librtmp",
      "--without-zsh-functions-dir",
      "--without-fish-functions-dir",
      "--disable-mqtt",
      "--without-libssh2",
      "--without-nghttp2",
      "--with-zlib=#{install_dir}/embedded",
      "--without-ca-path",
      "--without-ca-bundle",
      "--with-ca-fallback",
      "--with-openssl#{openssl_library_path}"
    ]
    command "autoreconf -fi", env: env, chdir: dest
    command configure_cmd.join(" "), env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
