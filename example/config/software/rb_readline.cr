require "../harness"

Omnibus.software :rb_readline do
  dependency :ruby

  build do
    env = with_embedded_path

    command "git clone https://github.com/ConnorAtherton/rb-readline.git #{source_dir}/rb-readline"
    command "ruby setup.rb", chdir: "#{source_dir}/rb-readline", env: env
  end
end
