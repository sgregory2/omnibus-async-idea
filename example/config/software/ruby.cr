require "../harness"

Omnibus.software :ruby do
  dependency :zlib
  dependency :openssl
  dependency :libffi
  dependency :libyaml
  dependency :libiconv

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    env["CFLAGS"] ||= "-I/var/opt/omnibus/embedded/include -march=native -O3 -g -pipe -fno-omit-frame-pointer #{env["CLFAGS"]?}"
    env["RUBY_CFLAGS"] = "-O3 #{env["RUBY_CFLAGS"]?}"

    puts env

    version = "2.7.5"
    url = "https://cache.ruby-lang.org/pub/ruby/2.7/ruby-#{version}.tar.gz"
    dest = "#{source_dir}/ruby"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    target = "x86_64-linux-gnu"

    config_cmd = String.build do |io|
      io << "./configure --with-out-ext=dbm,readline "
      io << "--enable-shared --disable-install-doc --without-gmp "
      io << "--without-gdbm --without-tk --disable-dtrace "
      io << "--with-opt-dir=#{install_dir}/embedded "
      io << "--build=#{target} --host=#{target} --target=#{target}"
    end

    command config_cmd, env: env, chdir: dest
    command "make -j 33", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
