require "../harness"

Omnibus.software :libre2 do
  build do
    env = with_standard_compiler_flags(with_embedded_path)
    source_url = "https://github.com/google/re2.git"
    dest = "#{source_dir}/re2"

    command "git clone #{source_url} #{dest}"
    command "make", env: env, chdir: dest
    command "make install prefix=#{install_dir}/embedded", env: env, chdir: dest
  end
end
