require "../harness"

Omnibus.software :libicu do
  build do
    version = "release-57-1"
    url = "https://github.com/unicode-org/icu/releases/download/release-57-1/icu4c-57_1-src.tgz"
    dest = "#{source_dir}/icu"
    config_dest = "#{dest}/source"

    env = with_standard_compiler_flags(with_embedded_path)
    env["LD_RPATH"] = "#{install_dir}/embedded/lib"

    wget url, "#{dest}.tgz"
    tar_xvf "#{dest}.tgz", dest

    configure_cmd = String.build do |io|
      io << "./runConfigureICU Linux/gcc "
      io << "--prefix=#{install_dir}/embedded "
      io << "--with-data-packaging=files "
      io << "--enable-shared --without-samples"
    end

    command configure_cmd, env: env, chdir: config_dest
    command "make -j 4", env: env, chdir: config_dest
    command "make install", env: env, chdir: config_dest
  end
end
