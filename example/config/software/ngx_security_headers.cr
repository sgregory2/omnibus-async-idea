require "../harness"

Omnibus.software :ngx_security_headers do
  build do
    url = "https://github.com/GetPageSpeed/ngx_security_headers/archive/refs/tags/0.0.9.tar.gz"
    dest = "#{source_dir}/ngx_security_headers"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest
  end
end
