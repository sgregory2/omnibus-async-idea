require "../harness"
require "../prometheus_flags"

Omnibus.software :postgres_exporter do
  build do
    version = "1.33.0"
    url = "https://github.com/prometheus-community/postgres_exporter/archive/refs/tags/v0.11.0.tar.gz"
    dest = "#{source_dir}/postgres_exporter"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = { "GOPATH" => dest }

    ldflags = [
      "-X main.Version=#{version}",
      "-s",
      "-w"
    ].join(" ")

    command "go build -ldflags '#{ldflags}' ./cmd/postgres_exporter", env: env, chdir: dest
    command "cp postgres_exporter #{install_dir}/embedded/bin/.", chdir: dest
  end
end
