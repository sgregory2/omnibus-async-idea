require "../harness"

Omnibus.software :mattermost do
  build do
    version = "7.2.0"
    url = "https://releases.mattermost.com/#{version}/mattermost-team-#{version}-linux-amd64.tar.gz"
    dest = "#{source_dir}/mattermost"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "mv bin/mattermost #{install_dir}/embedded/bin/mattermost", chdir: dest
    command "mv bin/mmctl #{install_dir}/embedded/bin/mmctl", chdir: dest
    mkdir "#{install_dir}/embedded/service/mattermost"
    command "cp -Rf ./* #{install_dir}/embedded/serice/mattermost/.", chdir: dest
  end
end
