require "../harness"

Omnibus.software :gnupg do
  dependency :libassuan
  dependency :npth
  dependency :libgcrypt
  dependency :libksba
  dependency :zlib
  dependency :bzip2

  build do
    version = "2.2.23"
    url = "https://www.gnupg.org/ftp/gcrypt/gnupg/gnupg-#{version}.tar.bz2"
    dest = "#{source_dir}/gnupg"

    wget url, "#{dest}.tar.bz2"
    tar_xvf "#{dest}.tar.bz2", dest

    env = with_standard_compiler_flags(with_embedded_path)
    env["LDFLAGS"] += " -lrt"

    command "./configure --prefix=#{install_dir}/embedded --disable-doc --without-readline --disable-sqlite --disable-gnutls --disable-dirmngr", env: env, chdir: dest
    command "make -j 10", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
