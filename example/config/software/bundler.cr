require "../harness"

Omnibus.software :bundler do
  dependency :ruby

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    command "gem install bundler --no-document --force", env: env
  end
end
