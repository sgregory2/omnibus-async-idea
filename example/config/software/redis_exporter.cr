require "../harness"
require "../prometheus_flags"

Omnibus.software :redis_exporter do
  build do
    version = "1.33.0"
    url = "https://github.com/oliver006/redis_exporter/archive/refs/tags/v1.33.0.tar.gz"
    dest = "#{source_dir}/redis_exporter"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = {
      "GOPATH" => dest,
      "GO111MODULE" => "on"
    }

    ldflags = [
      "-X main.BuildVersion=#{version}",
      "-X main.BuildDate=''",
      "-X main.BuildCommitSha=''",
      "-s",
      "-w"
    ].join(" ")

    command "go build -ldflags '#{ldflags}'", env: env, chdir: dest
    command "cp redis_exporter #{install_dir}/embedded/bin/", chdir: dest
  end
end
