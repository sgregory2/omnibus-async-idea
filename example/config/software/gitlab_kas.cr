require "../harness"

Omnibus.software :gitlab_kas do
  build do
    url = "https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/archive/v15.2.0/gitlab-agent-v15.2.0.tar.gz"
    dest = "#{source_dir}/gitlab_kas"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = { "TARGET_DIRECTORY" => "#{dest}/build" }

    command "make kas", env: env, chdir: dest
    command "mv build/kas #{install_dir}/embedded/bin/gitlab-kas", chdir: dest
  end
end
