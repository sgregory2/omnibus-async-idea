require "../harness"

Omnibus.software :libpng do
  dependency :zlib

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    url = "https://sourceforge.net/projects/libpng/files/libpng16/1.6.37/libpng-1.6.37.tar.gz/download"
    dest = "#{source_dir}/libpng"
    wget url, "#{source_dir}/libpng.tar.gz"
    tar_xvf "#{source_dir}/libpng.tar.gz", dest

    command "./configure --prefix=#{install_dir}/embedded --with-zlib=#{install_dir}/embedded", env: env, chdir: dest
    command "make", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
