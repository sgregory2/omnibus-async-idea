require "../harness"

Omnibus.software :spamcheck do
  dependency :libtensorflow_lite # isn't building

  build do
    version = "0.3.0"
    url = "https://gitlab.com/gitlab-org/spamcheck/-/archive/v0.3.0/spamcheck-v0.3.0.tar.gz"
    dest = "#{source_dir}/spamcheck"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags(with_embedded_path)
    env["GOPATH"] = dest
    env["PATH"] = "#{env["PATH"]}:#{env["GOPATH"]}/bin"

    env["CGO_CFLAGS"] = env["CFLAGS"].dup
    env["CGO_CPPFLAGS"] = env["CPPFLAGS"].dup
    env["CGO_CXXFLAGS"] = env["CXXFLAGS"].dup
    env["CGO_LDFLAGS"] = env["LDFLAGS"].dup

    command "make build", env: env, chdir: dest
    command "mv spamcheck #{install_dir}/embedded/bin/spamcheck", env: env, chdir: dest
  end
end
