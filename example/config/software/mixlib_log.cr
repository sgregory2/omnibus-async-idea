require "../harness"

Omnibus.software :mixlib_log do
  dependency :ruby

  build do
    version = "3.0.9"
    env = with_standard_compiler_flags(with_embedded_path)

    command "gem install mixlib-log --version #{version} --bindir '#{install_dir}/embedded/bin' --no-document", env: env
  end
end
