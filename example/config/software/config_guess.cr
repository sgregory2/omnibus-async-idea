require "../harness"

Omnibus.software :config_guess do
  build do
    mkdir "#{install_dir}/embedded/lib/config_guess"

    wget "https://raw.githubusercontent.com/gcc-mirror/gcc/master/config.guess", "#{install_dir}/embedded/lib/config_guess/config.guess"
    wget "https://raw.githubusercontent.com/gcc-mirror/gcc/master/config.sub", "#{install_dir}/embedded/lib/config_guess/config.sub"
  end
end
