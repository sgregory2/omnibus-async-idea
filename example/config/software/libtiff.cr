require "../harness"

Omnibus.software :libtiff do
  dependency :libtool
  dependency :zlib
  dependency :liblzma
  dependency :libjpeg_turbo
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    source_url = "https://download.osgeo.org/libtiff/tiff-4.4.0.tar.xz"
    dest = "#{source_dir}/libtiff"

    wget source_url, "#{dest}.tar.xz"
    tar_xvf "#{dest}.tar.xz", dest

    command "./autogen.sh", env: env, chdir: dest
    command "./configure --disable-zstd --prefix=#{install_dir}/embedded", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
