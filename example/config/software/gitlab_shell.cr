require "../harness"

Omnibus.software :gitlab_shell do
  dependency :ruby

  build do
    url = "https://gitlab.com/gitlab-org/gitlab-shell/-/archive/v14.10.0/gitlab-shell-v14.10.0.tar.gz"
    dest = "#{source_dir}/gitlab_shell"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    env = with_standard_compiler_flags(with_embedded_path)
    mkdir "#{install_dir}/embedded/service/gitlab-shell"
    command "make build", env: env, chdir: dest
    command "cp -Rf ./* #{install_dir}/embedded/service/gitlab-shell/.", chdir: dest
  end
end
