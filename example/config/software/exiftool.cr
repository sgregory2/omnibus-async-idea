require "../harness"

Omnibus.software :exiftool do
  build do
    url = "https://github.com/exiftool/exiftool/archive/refs/tags/12.42.tar.gz"
    dest = "#{source_dir}/exiftool"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    mkdir "#{install_dir}/embedded/bin"
    command "cp -Rf lib #{install_dir}/embedded/lib/exiftool-perl", chdir: dest
    command "cp exiftool #{install_dir}/embedded/bin/exiftool", chdir: dest
  end
end
