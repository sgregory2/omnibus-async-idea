require "../harness"

Omnibus.software :libyaml do
  dependency :config_guess
  dependency :libtool

  build do
    version = "0.1.7"
    source_url = "http://pyyaml.org/download/libyaml/yaml-#{version}.tar.gz"
    env = with_standard_compiler_flags(with_embedded_path)

    dest = "#{source_dir}/yaml"

    wget source_url, "#{source_dir}/yaml.tar.gz"
    tar_xvf "#{source_dir}/yaml.tar.gz", dest

    command "./configure --enable-shared", env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
