require "../harness"

Omnibus.software :pkg_config_lite do
  dependency :config_guess

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "0.28-1"
    url = "http://downloads.sourceforge.net/project/pkgconfiglite/#{version}/pkg-config-lite-#{version}.tar.gz"
    dest = "#{source_dir}/pkg_config_lite"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.bz", dest

    command "./configure --prefix=#{install_dir}/embedded --disable-host-tool --with-pc-path=#{install_dir}/embedded/bin/pkgconfig", env: env, chdir: dest
    command "make -j 7", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
