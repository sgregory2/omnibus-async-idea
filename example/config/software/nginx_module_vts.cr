require "../harness"

Omnibus.software :nginx_module_vts do
  build do
    url = "https://github.com/vozlt/nginx-module-vts/archive/refs/tags/v0.1.18.tar.gz"
    dest = "#{source_dir}/nginx_module_vts"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest
  end
end
