require "../harness"

Omnibus.software :gitaly do
  dependency :pkg_config_lite
  dependency :ruby
  dependency :bundler
  dependency :libicu
  dependency :git

  build do
    env = with_standard_compiler_flags(with_embedded_path)
    url = "https://gitlab.com/gitlab-org/gitaly/-/archive/v15.1.5/gitaly-v15.1.5.tar.gz"
    dest = "#{source_dir}/gitaly"
    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest
    bundle_without = %w(development test)
    ruby_dest = "#{dest}/ruby"
    command "bundle config set --local frozen 'true'"
    command "bundle config build.nokogiri --use-system-libraries --with-xml2-include=#{install_dir}/embedded/include/libxml2 --with-xslt-include=#{install_dir}/embedded/include/libxslt", env: env
    command "bundle install --without #{bundle_without.join(" ")}", env: env, chdir: ruby_dest

    ruby_install_dir = "#{install_dir}/embedded/service/gitaly-ruby"
    mkdir ruby_install_dir
    command "cp -Rf ./ruby/* #{ruby_install_dir}/.", chdir: dest
    command "make install PREFIX=#{install_dir}/embedded", env: env, chdir: dest
  end
end
