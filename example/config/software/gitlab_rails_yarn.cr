require "../harness"

Omnibus.software :gitlab_rails_yarn do
  dependency :gitlab_rails

  build do
    dest = "#{source_dir}/gitlab_rails"
    env = with_standard_compiler_flags(with_embedded_path)
    env = {
      "NODE_ENV" => "production",
      "RAILS_ENV" => "production",
      "PATH" => "#{install_dir}/embedded/bin:#{ENV["PATH"]}",
      "USE_DB" => "false",
      "SKIP_STORAGE_VALIDATION" => "true",
      "NODE_OPTIONS" => "--max_old_space_size=10584",
      "NO_SOURCEMAPS" => "true"
    }
    # assets_compile_env["NO_SOURCEMAPS"] = "true" if Gitlab::Util.get_env("NO_SOURCEMAPS")
    command "yarn install --pure-lockfile --production", env: env, chdir: dest
    command "rake -f ./lib/tasks/yarn.rake yarn:check", env: env, chdir: dest
    command "yarn webpack", env: env, chdir: dest
  end
end
