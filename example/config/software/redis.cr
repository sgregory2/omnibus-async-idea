require "../harness"

Omnibus.software :redis do
  dependency :config_guess
  dependency :openssl

  build do
    env = with_standard_compiler_flags(with_embedded_path).merge({
      "PREFIX" => "#{install_dir}/embedded"
    })
    env["CFLAGS"] += " -fno-omit-frame-pointer"

    version = "6.2.7"
    url = "https://download.redis.io/releases/redis-6.2.7.tar.gz"
    dest = "#{source_dir}/redis"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "make -j 33 BUILD_TLS=yes", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
