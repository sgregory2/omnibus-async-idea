require "../harness"

Omnibus.software :spam_classifier do
  build do
    version = "0.3.0"
    url = "https://glsec-spamcheck-ml-artifacts.storage.googleapis.com/spam-classifier/#{version}/linux.tar.gz"
    dest = "#{source_dir}/spam_classifier"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    mkdir "#{install_dir}/embedded/service/spam-classifier"
    command "cp -R ./* #{install_dir}/embedded/service/spam-classifier/.", chdir: dest
    command "cp -Rf dist #{install_dir}/embedded/service/spam-classifier/preprocessor", chdir: dest
    command "cp tokenizer.pickle #{install_dir}/embedded/service/spam-classifier/preprocessor/", chdir: dest
  end
end
