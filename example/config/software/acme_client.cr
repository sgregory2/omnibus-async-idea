require "../harness"

Omnibus.software :acme_client do
  dependency :ruby

  build do
    version = "2.0.11"
    env = with_standard_compiler_flags(with_embedded_path)
    command "gem install acme-client --no-document --version #{version}", env: env
  end
end
