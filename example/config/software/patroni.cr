require "../harness"

Omnibus.software :patroni do
  dependency :python3
  dependency :psycopg2

  build do
    version = "2.1.0"
    env = with_standard_compiler_flags(with_embedded_path)

    command "#{install_dir}/embedded/bin/pip3 install prettytable==0.7.2", env: env
    command "#{install_dir}/embedded/bin/pip3 install patroni[consul]==#{version}", env: env
  end
end
