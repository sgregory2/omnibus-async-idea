require "../harness"

Omnibus.software :gitlab_cookbooks do
  build do
    cookbook_name = "gitlab"
    mkdir "#{install_dir}/embedded/cookbooks"
    command "cp -Rf /vendor/gitlab-cookbooks #{install_dir}/embedded/cookbooks/"
    # about as far as we are going for now
  end
end
