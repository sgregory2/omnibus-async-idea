require "../harness"

Omnibus.software :gitlab_pg_ctl do
  build do
    foo = install_dir
    block do
      File.open("#{foo}/embedded/bin/gitlab-pg-ctl", "w") do |file|
        file.print <<-EOH
        #!/bin/sh

        error_echo()
        {
          echo "$1" 2>& 1
        }

        gitlab_psql_rc='/opt/gitlab/etc/gitlab-psql-rc'


        if ! [ -f ${gitlab_psql_rc} ] || ! [ -r ${gitlab_psql_rc} ] ; then
          error_echo "$0 error: could not load ${gitlab_psql_rc}"
          error_echo "Either you are not allowed to read the file, or it does not exist yet."
          error_echo "You can generate it with:   sudo gitlab-ctl reconfigure"
          exit 1
        fi

        . "${gitlab_psql_rc}"

        if [ "$(id -n -u)" = "${psql_user}" ] ; then
          privilege_drop=''
        else
          privilege_drop="-u ${psql_user}:${psql_group}"
        fi

        export PGDATA=${psql_host}/data
        cd /tmp; exec /opt/gitlab/embedded/bin/chpst ${privilege_drop} /opt/gitlab/embedded/bin/pg_ctl "$@"
      EOH
      end
    end

    command "chmod 755 #{install_dir}/embedded/bin/gitlab-pg-ctl"
  end
end
