require "../harness"

Omnibus.software :libffi do
  dependency :libtool

  build do
    env = with_standard_compiler_flags(with_embedded_path)

    version = "3.2.1"
    url = "https://sourceware.org/pub/libffi/libffi-#{version}.tar.gz"
    dest = "#{source_dir}/libffi"

    wget url, "#{dest}.tar.gz"
    tar_xvf "#{dest}.tar.gz", dest

    command "./configure", env: env, chdir: dest
    command "make -j 5", env: env, chdir: dest
    command "make install", env: env, chdir: dest
  end
end
